// Part 1
// const name = "Regina";
// const url = `images/${name.toLowerCase()}.jpg`;
// console.log(url);

// Part 2
// let html; 
// html = '<article class="pizzaThumbnail"> <a href="' + url + '"> <img src="'+ url+'"/> <section>Regina</section> </a> </article>';
// document.querySelector('.pageContent').innerHTML = html;

// Part 3
// let idx = 0;
// let tab = '';
// const data = ['Regina', 'Napolitaine', 'Spicy'];
// data.forEach(String => {
//     tab += `<article class="pizzaThumbnail"> <a href="images/${data[idx].toLowerCase()}.jpg"> <img src="images/${data[idx].toLowerCase()}.jpg"/> <section>${data[idx]}</section> </a> </article>`;
//     idx++;
// });
// document.querySelector('.pageContent').innerHTML = tab;

// Part 4
const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
    }];
    data.sort(function(a,b){
        if(a.price_small!=b.price_small){
            return a.price_small>b.price_large;
        }else{
            return a.price_large>b.price_large;
        }
    });
    let tab = '';
    data.forEach( function(toto)  {
        tab += 
        `<article class="pizzaThumbnail"> 
            <a href="${toto.image}.jpg"> 
                <img src="${toto.image}.jpg"/> 
                <section> 
                    <h4>${toto.name} </h4>
                    <ul> 
                        <li>Prix petit format : ${toto.price_small} €</li> 
                        <li>Prix grand format : ${toto.price_large} €</li> 
                    </ul> 
                </section> 
            </a> 
        </article>`;
    });
    document.querySelector('.pageContent').innerHTML = tab;





 