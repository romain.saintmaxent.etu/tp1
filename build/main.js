"use strict";

// Part 1
// const name = "Regina";
// const url = `images/${name.toLowerCase()}.jpg`;
// console.log(url);
// Part 2
// let html; 
// html = '<article class="pizzaThumbnail"> <a href="' + url + '"> <img src="'+ url+'"/> <section>Regina</section> </a> </article>';
// document.querySelector('.pageContent').innerHTML = html;
// Part 3
// let idx = 0;
// let tab = '';
// const data = ['Regina', 'Napolitaine', 'Spicy'];
// data.forEach(String => {
//     tab += `<article class="pizzaThumbnail"> <a href="images/${data[idx].toLowerCase()}.jpg"> <img src="images/${data[idx].toLowerCase()}.jpg"/> <section>${data[idx]}</section> </a> </article>`;
//     idx++;
// });
// document.querySelector('.pageContent').innerHTML = tab;
// Part 4
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
data.sort(data.price_large);
var tab = '';
data.forEach(function (toto) {
  tab += "<article class=\"pizzaThumbnail\"> \n            <a href=\"".concat(toto.image, ".jpg\"> \n                <img src=\"").concat(toto.image, ".jpg\"/> \n                <section> \n                    <h4>").concat(toto.name, " </h4>\n                    <ul> \n                        <li>Prix petit format : ").concat(toto.price_small, " \u20AC</li> \n                        <li>Prix grand format : ").concat(toto.price_large, " \u20AC</li> \n                    </ul> \n                </section> \n            </a> \n        </article>");
});
document.querySelector('.pageContent').innerHTML = tab;